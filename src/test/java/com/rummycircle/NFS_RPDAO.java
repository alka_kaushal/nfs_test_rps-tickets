package com.rummycircle;


import com.rummycircle.utils.database.AbstractDAO;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class NFS_RPDAO extends AbstractDAO{
	
	private JdbcTemplate jdbcTemplate;
	 
	

	public NFS_RPDAO() {
		
		this.jdbcTemplate = new JdbcTemplate(getDataSource());
		
	}
	
	public int getRPfromNFT(long userId) {
		Integer result= null;
		String query = String.format("select rp_balance from nft_player_accounts where user_id="+userId);
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);
		    
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
	
	public int getRPfromPlayer_RP(long userId) {
		Integer result= null;
		String query = String.format("select updated_balance from player_rp_transactions where user_id="+userId+" order by update_time desc limit 1");
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class); 
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
	
	public int getRPfromMonthly_EntryFee(long userId) {
		Integer result= null;
		String query = String.format("select reward_points from monthly_entry_fee_history where userid="+userId+" order by update_date desc limit 1");
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);
		    
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
}
	
		


