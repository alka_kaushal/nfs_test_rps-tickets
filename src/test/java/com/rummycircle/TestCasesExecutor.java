package com.rummycircle;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.restclient.ResponseValidator;
import com.rummycircle.utils.config.ConfigManager;
import com.rummycircle.utils.config.ConfigProperties;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.RestTestFileYAMLHelper.TestCase;
/**
 * @author nishant Kashyap - nishant.kashyap@games24x7.com
 * @since December 2015
 *
 */ 
public class TestCasesExecutor extends BaseTest {
	
	
	private static Logger Log = Logger.getLogger(TestCasesExecutor.class);
	
	
	@Test(dataProvider = "tests")
	public void testExecuteymlfile( TestCase tc) {
		
		int expectedStatusCode = tc.getExpectedStatusCode();

		HTTPResponse resp = getResponseForTestCase(tc);
		
		Log.info("Response status code: " +resp.getStatusCode());
		Assert.assertTrue(resp.getStatusCode()==expectedStatusCode);

		 boolean check = ResponseValidator.isResponseEmpty(resp);
		  if(check) {
			  Log.info("Response is EMPTY!!");
		  }else {
			  Log.info("Response body : " +resp.getBody().getBodyText());
		  }
		  				
		if(null!=tc.getResponseToValidate()) {
			Log.info("Validate Response using json path keys");
			ResponseValidator.validateResponse(resp.getBody().getBodyText(), tc.getResponseToValidate());
		}		
		
	}
	
	
	
	@DataProvider(name="tests")
	private Object[][] generateTestCases() throws Exception {
		
		return getDataProviderDataForTestCases(ConfigManager.getInstance().getString(ConfigProperties.TEST_FILENAME.getKey()));
		
	}
	

}

