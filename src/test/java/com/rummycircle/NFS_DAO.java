package com.rummycircle;


import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.rummycircle.utils.database.AbstractDAO;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.RowMapper;
import com.rummycircle.utils.testutils.PropertyReader;


@Component
public class NFS_DAO extends AbstractDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	Properties prop = PropertyReader.loadCustomProperties("custom.properties");
	String tick=prop.getProperty("TICKETID");
	int ticketid=Integer.parseInt(tick);
	 
	

	public NFS_DAO() {
		
		this.jdbcTemplate = new JdbcTemplate(getDataSource());
		
	}
	
	public int getRPfromNFT(long userId) {
		Integer result= null;
		String query = String.format("select rp_balance from nft_player_accounts where user_id="+userId);
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);
		    
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
	
	public int getRPfromPlayer_RP(long userId) {
		Integer result= null;
		String query = String.format("select updated_balance from player_rp_transactions where user_id="+userId+" order by update_time desc limit 1");
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class); 
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
	
	public int getRPfromMonthly_EntryFee(long userId) {
		Integer result= null;
		String query = String.format("select reward_points from monthly_entry_fee_history where userid="+userId+" order by update_date desc limit 1");
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);
		    
			
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}
	
	
	
	// DAO for Tickets
	
	
	public List<List<Object>> getTicketDetailsByID(){
		String query="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
		
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
		
	}//end of ticketDetailsByID
	
	
	public int getCount() {
		String query = String.format("select count(id) from ticket_creation_details;");
		
		Integer result= null;
		
		try {
			
		    result =  jdbcTemplate.queryForObject(query, Integer.class);
		    return result;

		     } catch (EmptyResultDataAccessException e) {
		return 0;
		}
		
		}
	
	
	

	public List<List<Object>> getTicketDetails() {
		String query = "select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details order by id;";
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}// end of getTicketDetails
	
	public List<List<Object>> getTickDetailsAfterCreate(int ticketid){
		String query="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
		
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
		
	}//end of ticketDetailsByID
	
	
	public List<List<Object>> getAssignedTicket(int ticketId){
//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
	String query="select userid,status from ticket_users_map where ticket_id="+ticketId+" order by id desc limit 10";	
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("userid"));
					list.add(1, rs.getInt("status"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
		
	}//end of getAssignedTicket
}
	
		


