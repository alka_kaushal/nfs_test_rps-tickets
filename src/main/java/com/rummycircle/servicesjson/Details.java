package com.rummycircle.servicesjson;

import java.util.Arrays;

public class Details {
	
	int ticketId;
	int noOfTickets;
	String comments;
	String ticketType;
	long userIds[];
	String endDate;
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(int noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public long[] getUserIds() {
		return userIds;
	}
	public void setUserIds(long[] userIds) {
		this.userIds = userIds;
	}
	
	@Override
	public String toString() {
		return "AssignDetails [ticketId=" + ticketId + ", noOfTickets="
				+ noOfTickets + ", comments=" + comments + ", ticketType="
				+ ticketType + ", userIds=" + Arrays.toString(userIds) + "]";
	}

	
	

}
