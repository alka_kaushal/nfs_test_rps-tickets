package com.rummycircle;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.servicesjson.CreditOnVerificationServiceClass;
import com.rummycircle.servicesjson.RPCreditServiceClass;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.PropertyReader;


public class RP_UPDATE_NFS extends BaseTest {
	
	NFS_RPDAO dbObj = new NFS_RPDAO();

	private static Logger Log = Logger.getLogger(RP_UPDATE_NFS.class);
	
	
	
	
	@Test
	public void JOIN_TOURNAMENT() {
		Properties prop = PropertyReader.loadCustomProperties("custom.properties");

		String path = ServicesEndPoint.JOIN_TOURNAMENT;
		

		RPCreditServiceClass serviceObj = new RPCreditServiceClass();
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Long.parseLong(prop.getProperty("tournamentId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty("rewardPointValue").trim()));
	
		int initialgetRPfromNFT = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		int initialgetRPfromMonthly_EntryFee = dbObj.getRPfromMonthly_EntryFee(Long.parseLong(prop.getProperty("userId").trim()));
		
		
		HTTPRequest request = new HTTPRequest(serviceObj);
		
		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
		Log.info("response body:"+ httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());
		
		int getRPfromNFT= dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		int getRPfromMonthly_EntryFee = dbObj.getRPfromMonthly_EntryFee(Long.parseLong(prop.getProperty("userId").trim()));
		
		Assert.assertEquals((initialgetRPfromNFT-getRPfromNFT),serviceObj.getRewardPointValue() );
		Assert.assertEquals((initialgetRPfromMonthly_EntryFee-getRPfromMonthly_EntryFee), serviceObj.getRewardPointValue() );
		
		

	}
	
	//@Test
	public void WITHDRAW_TOURNAMENT(){
		
		Properties prop = PropertyReader.loadCustomProperties("custom.properties");

		String path = ServicesEndPoint.WITHDRAW_TOURNAMENT;

		RPCreditServiceClass serviceObj = new RPCreditServiceClass();
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Long.parseLong(prop.getProperty("tournamentId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty("rewardPointValue").trim()));
		
		int initialresultDB = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		
		HTTPRequest request = new HTTPRequest(serviceObj);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
		
		int resultDB= dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		Assert.assertEquals((resultDB-initialresultDB),serviceObj.getRewardPointValue() );
		
		Log.info("response body: "+httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());
		
		
		
	}
	
	//@Test
	public void CREDIT_RP_VERIFICATION(){

		Properties prop = PropertyReader.loadCustomProperties("custom.properties");

		String path = ServicesEndPoint.CREDIT_RP_ON_VERIFICATION;

		CreditOnVerificationServiceClass serviceObj = new CreditOnVerificationServiceClass();
		
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTxnId(Long.parseLong(prop.getProperty("txnId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty("rewardPointValue").trim()));

		int initialresultDB = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		int initialGetPlayerRp = dbObj.getRPfromPlayer_RP(Long.parseLong(prop.getProperty("userId").trim()));		
		HTTPRequest request = new HTTPRequest(serviceObj);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
		
		int resultDB= dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		int finalGetPlayerRp = dbObj.getRPfromPlayer_RP(Long.parseLong(prop.getProperty("userId").trim()));		
		
		Assert.assertEquals((resultDB-initialresultDB),serviceObj.getRewardPointValue() );
		Assert.assertEquals((finalGetPlayerRp-initialGetPlayerRp),serviceObj.getRewardPointValue());
		
		Log.info("response body: "+ httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());
 		
	
	}
	
//	@Test
	public void CREDIT_RP_POSTSETTLEMENT(){
	
		Properties prop = PropertyReader.loadCustomProperties("custom.properties");

		String path = ServicesEndPoint.CREDIT_RP_POST_SETTLEMENT;

		RPCreditServiceClass serviceObj = new RPCreditServiceClass();
		
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Long.parseLong(prop.getProperty("tournamentId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty("rewardPointValue").trim()));
		serviceObj.setNewrpSpendCounter(Double.parseDouble(prop.getProperty("newrpSpendCounter").trim()));
		
		int initialresultDB = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		
		HTTPRequest request = new HTTPRequest(serviceObj);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
		
		int resultDB= dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
		Assert.assertEquals((resultDB-initialresultDB),serviceObj.getRewardPointValue() );
		
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());
		Log.info("response body:" + httpResp.getBody().getBodyText());
	
	}
	
	
}

